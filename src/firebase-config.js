import { initializeApp } from "firebase/app";
import { getAuth, GoogleAuthProvider } from "firebase/auth";
import { getFirestore } from "firebase/firestore";
import { getAnalytics } from "firebase/analytics";

const firebaseConfig = {
  apiKey: "AIzaSyCyV8dpz98_mdMo4QuDcrvySHQv4ILi4aM",
  authDomain: "chatverse-d92b4.firebaseapp.com",
  projectId: "chatverse-d92b4",
  storageBucket: "chatverse-d92b4.appspot.com",
  messagingSenderId: "405473324817",
  appId: "1:405473324817:web:2474b2613b18222639a6b0",
  measurementId: "G-DFMVXE0KZJ",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export const auth = getAuth(app);
export const provider = new GoogleAuthProvider();
export const db = getFirestore(app);
